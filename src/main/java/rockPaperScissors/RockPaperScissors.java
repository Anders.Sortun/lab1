package rockPaperScissors;

import java.util.Random;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    

    public static int getRandomNumber(){
        Random rand = new Random();
        return rand.nextInt(3);
    }

    public static boolean isWinner(String choice1, String choice2){
        if ((choice1.equals("rock") && (choice2.equals("scissors")))){
            return true;
        } else if ((choice1.equals("paper") && (choice2.equals("rock")))){
            return true;
        } else if ((choice1.equals("scissors") && (choice2.equals("paper")))){
            return true;
        }
        return false;
    }

    public String humanChoice(){
        while (true) {
            System.out.println("Your choice (Rock/Paper/Scissors)?");
            String humanChoice = sc.nextLine();

            if (humanChoice.equals("rock") || humanChoice.equals("paper") || humanChoice.equals("scissors")){
                return humanChoice;
            } else {
                System.out.println("I do not understand " + humanChoice + ". Could you try again?");
                continue;
            }  
        }
    }

    public void run() {
        // TODO: Implement Rock Paper Scissors
        
            boolean leave = false;
    
            while (true) {
                System.out.println("Let's play round " + roundCounter);
    
                String humanChoice = humanChoice();
                
                String computerChoice = rpsChoices.get(getRandomNumber());
            
                System.out.print("Human chose " + humanChoice + ", computer chose " + computerChoice + ". ");
    
                if (isWinner(humanChoice,computerChoice)){
                    System.out.println("Human wins!");
                    humanScore += 1;
                } else if (isWinner(computerChoice, humanChoice)) {
                    System.out.println("Computer wins!");
                    computerScore += 1;
                } else {
                    System.out.println("It's a tie!");
                }
    
                System.out.println("Score: human " + humanScore + ", computer " + computerScore);
    
    
                while (true){  
                    System.out.println("Do you wish to continue playing? (y/n)?");
                    String ans = sc.nextLine();
                    if (ans.equals("n")){
                        System.out.println("Bye bye :)");
                        leave = true;
                        break;
                    } else if (ans.equals("y")){
                        roundCounter += 1;
                        leave = false;
                        break;
                    } else {
                        System.out.println("Sorry didn´t get that. Please try again.");
                        continue;
                    }
                
                }
            
                
                if (leave){
                    break;
                }
            }
    
        
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
